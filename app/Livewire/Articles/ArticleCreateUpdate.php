<?php

namespace App\Livewire\Articles;

use App\Models\Article;
use Livewire\Component;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
// use Illuminate\Http;
use Livewire\WithFileUploads;
use Illuminate\Validation\Rule;
// use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class ArticleCreateUpdate extends Component
{
    use WithFileUploads;

    public Article $article;

    protected function rules() 
    {
        return [
            // 'file' => 'nullable|mimes:jpg,bmp,png',
            'article.title' => 'required|min:7',
            'article.slug'  => [
               'alpha_dash', 
                Rule::unique('articles', 'slug')->ignore($this->article),
            ], 
            'article.body'  => 'required|min:7|max:65,535',
        ];
    }

    public function updated($property)
    {
        $this->validateOnly($property);
    }
    
    public function updatedArticleTitle($title)
    {
        $this->article->slug = Str::slug($title);
    }

    public function mount(Article $article)
    {
        // dd($this->article);
        $this->article = $article;
    }

    public function render()
    {
        return view('livewire.articles.article-create-update');
    }

    public function save(Request $request)
    {
        $this->validate();

        $file = $request->file('file');
        $file_path = $file->getPathName();

        $response = Http::withHeaders([
            'authorization' => 'Client-ID ' . config('services.imgur.client_id'),
            'content-type' => 'application/x-www-form-urlencoded',
        ])->send('POST', 'https://api.imgur.com/3/image', [
            'form_params' => [
                    'image' => base64_encode(file_get_contents($request->file('file')->path($file_path)))
                ],
            ]);
        $data['file'] = data_get(response()->json(json_decode(($response->getBody()->getContents())))->getData(), 'data.link');

        // dd($response);

        Auth::user()->articles()->save($this->article);

        // $this->article->save();

        session()->flash('msg', __('Article saved'));

        return to_route('article.index');
        // dd($this->article);
    }
}
