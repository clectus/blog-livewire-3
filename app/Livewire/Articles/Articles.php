<?php

namespace App\Livewire\Articles;

use App\Models\Article;
use Livewire\Component;

class Articles extends Component
{
    public $search = '';
    public $user;


    public function render()
    {
        return view('livewire.articles.articles', [
            'articles' => Article::where('title', 'like', '%{$this->search}%')->latest()->get()
        ]);
    }
}
// ->layout('components.panel.panel-layout')