<div x-data="{ value: @entangle($attributes->wire('model')).defer }" x-on:trix-change="value = $event.target.value">

    <div wire:ignore>
        <trix-editor :value="value" {!! $attributes->whereDoesntStartWith('wire:model')->merge(['class' => 'block mt-3 mb-4 w-full  text-xl italic placeholder:text-xl ']) !!}></trix-editor>
    </div>
</div>
