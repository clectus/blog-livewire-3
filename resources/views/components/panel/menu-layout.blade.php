<nav class=" flex flex-col m-1 gap-2">
    <x-panel.menu-user/>
    <x-dropdown-link class="text-xl !p-4 bg-purple-950 text-white hover:bg-slate-600 italic" :href="route('article.index')">
        {{ __('Articles') }}
    </x-dropdown-link>
    <x-dropdown-link class="text-xl !p-4 bg-purple-950 text-white hover:bg-slate-600 italic" :href="route('dashboard')">
        {{ __('Dashboard') }}
    </x-dropdown-link>
    {{-- <x-dropdown-link class="text-xl !p-4 bg-purple-950 text-white hover:bg-slate-600 italic" :href="route('article.create')" wire:navigate>
        {{ __('Create article') }}
    </x-dropdown-link> --}}
    {{-- <a href="" class="bg-emerald-950 pl-4 py-2 shadow-md shadow-black hover:bg-gray-900">Enlace 2</a>
    <a href="" class="bg-emerald-950 pl-4 py-2 shadow-md shadow-black hover:bg-gray-900">Enlace 3</a>
    <a href="" class="bg-emerald-950 pl-4 py-2 shadow-md shadow-black hover:bg-gray-900">Enlace 4</a>
    <a href="" class="bg-emerald-950 pl-4 py-2 shadow-md shadow-black hover:bg-gray-900">Enlace 5</a> --}}
</nav>