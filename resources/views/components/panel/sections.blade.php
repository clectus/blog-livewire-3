<section class="grid grid-cols-11 gap-1">
    <article class="col-span-11 p-3 m-1 bg-slate-950 lg:col-span-8">
        {{ $slot }}
    </article>
    <article class="col-span-11 p-1 m-1 bg-slate-950 lg:col-span-3">
        <x-panel.menu-layout/>
    </article>
</section>