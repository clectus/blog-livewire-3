<x-panel.sections>
    <div class=" text-yellow-300 flex flex-col items-center gap-2">
        <span class=" bg-neutral-950 !w-full p-3 text-center shadow shadow-slate-500 flex flex-col gap-2 lg:flex-row items-center">
            <h3 class="w-full">{{ __('Create article') }}</h3>
            
        </span>
        <div class=" m-3 bg-slate-900  text-white p-3 shadow shadow-slate-900 w-full ">
            <form wire:submit="save" enctype="multipart/form-data"
                <x-input-label :value="__('Cover image') " class=" text-xl text-amber-300 italic "/>
                <div class="flex items-center justify-center w-full mt-3 mb-3">
                    <label for="file" class="flex flex-col items-center justify-center w-full h-64 border-2 border-gray-300  shadow shadow-black cursor-pointer bg-indigo-950 dark:hover:bg-bray-800 dark:bg-gray-700 hover:bg-gray-900 dark:border-gray-600 dark:hover:border-gray-500 dark:hover:bg-gray-600">
                        <div class="flex  flex-col items-center justify-center pt-5 pb-6">
                            <svg class="w-8 h-8 mb-4 text-gray-500 dark:text-gray-400" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 16">
                                <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 13h3a3 3 0 0 0 0-6h-.025A5.56 5.56 0 0 0 16 6.5 5.5 5.5 0 0 0 5.207 5.021C5.137 5.017 5.071 5 5 5a4 4 0 0 0 0 8h2.167M10 15V6m0 0L8 8m2-2 2 2"/>
                            </svg>
                            <p class="mb-2 text-white dark:text-gray-400 hover:text-red-300">
                                <span class="font-semibold">Dale click para cargar</span> 
                                o arrastre y suelta
                            </p>
                            <p class="text-xs text-blue-300 dark:text-gray-400">
                                [ SVG, PNG, JPG o GIF ...]
                            </p>
                        </div>
                        <input id="file" type="file" name="file" class="hidden" />
                    </label>
                </div> 

                <!-- Name -->
                <div>
                    <x-input-label for="title" :value="__('Title') " class=" text-xl text-amber-300 italic "/>
                    <x-text-input id="title" wire:model="article.title" class="block mt-3 mb-4 w-full !bg-transparent border-0 text-xl italic border-b-2 rounded-none placeholder:text-white placeholder:text-xl focus:ring-0 focus:border-[#48ee06]  border-[#39c9d3]" type="text" placeholder="Escribe el título"  :value="old('article.title')" autocomplete="title" />
                    <x-input-error :messages="$errors->get('article.title')" class="mt-2 text-xl italic" />
                </div>

                <div class="  ">
                    <x-input-label for="body" :value="__('Content') " class=" text-xl text-amber-300 italic mt-3 mb-3"/>

                    <x-panel.area placeholder="Escribe el contenido" wire:model="article.body" id="body"></x-panel.area>


                    {{-- <x-text-input id="body" wire:model="article.body" class="block mt-3 mb-4 w-full !bg-transparent border-0 text-xl italic border-b-2 rounded-none placeholder:text-white placeholder:text-xl focus:ring-0 focus:border-[#48ee06]  border-[#39c9d3]" type="text" placeholder="Pon el contenido del artículo"  :value="old('article.body')" autocomplete="body" /> --}}
                    <x-input-error :messages="$errors->get('article.body')" class="mt-2 text-xl italic" />
                </div>
        
            
                <div class="flex gap-1 flex-col lg:flex-row items-center justify-end mt-4">
                    <x-dropdown-link class="text-xl !p-4 bg-purple-950 text-white shadow shadow-stone-950 hover:bg-slate-600 italic" :href="route('article.index')">
                        {{ __('Cancel') }}
                    </x-dropdown-link>
                    {{-- <x-dropdown-link class="text-xl !p-4 bg-purple-950 text-white shadow shadow-stone-950 hover:bg-slate-600 italic cursor-pointer">
                        {{ __('Register') }}
                    </x-dropdown-link> --}}
                    <x-primary-button class="!normal-case !text-[1.24rem] rounded-none w-full !p-4 shadow shadow-stone-950">
                        {{ __('Register') }}
                    </x-primary-button>
                </div>
            </form>
        </div>
    </div>
</x-panel.sections>
