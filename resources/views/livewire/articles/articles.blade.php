<x-panel.sections>
    <div class=" text-yellow-300 flex flex-col items-center gap-2">
        <span class=" bg-neutral-950 !w-full p-3 text-center shadow shadow-slate-500 flex flex-col gap-2 lg:flex-row items-center">
            <h3 class="w-full">Listado de artículos</h3>
            <x-dropdown-link class="text-xl !p-4 bg-purple-950 text-white hover:bg-slate-600 italic" :href="route('article.create')">
                {{ __('Create article') }}
            </x-dropdown-link>
        </span>
        <x-text-input wire:model="search" class="block mt-3 mb-4 w-full !bg-transparent border-0 text-xl italic border-b-2 rounded-none placeholder:text-white placeholder:text-xl focus:ring-0 focus:border-[#48ee06]  border-[#39c9d3]" type="search" name="search" placeholder="Buscar 🤔"  :value="old('search')" autocomplete="search" />
        <div class="">
            <span class="">
                @if (session('msg'))
                    {{ session('msg') }}
                @endif
            </span>
            @forelse ($articles as $article)
                <li>{{ $article->title }}</li>
                <li>{{ $article->body }}</li>
            @empty
                <p>No hay {{ __('Articles') }} 😥</p>
            @endforelse
        </div>
    </div>
</x-panel.sections>
