<?php

use App\Livewire\Articles\ArticleCreateUpdate;
use App\Livewire\Articles\Articles;
use Illuminate\Support\Facades\Route;


Route::view('/', 'welcome');

// Route::view('dashboard', 'dashboard')
//     ->middleware(['auth', 'verified'])
//     ->name('dashboard');

// Route::view('profile', 'profile')
//     ->middleware(['auth'])
//     ->name('profile');

Route::middleware('auth')->prefix('tablero')->group(function () {

    // Tablero
    Route::view('', 'dashboard')->name('dashboard');
    
    // Perfil
    Route::view('perfil', 'profile')->name('profile');
    
    // Articulos
    Route::get('articulos', Articles::class)->name('article.index');
    Route::get('crear/articulo', ArticleCreateUpdate::class)->name('article.create');
    Route::get('editar/{article}', ArticleCreateUpdate::class)->name('article.edit');




});

require __DIR__.'/auth.php';
